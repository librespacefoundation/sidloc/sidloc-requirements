**Actor**

*Add primary actor of use case*

Examples:  
Satellite operator  
Constellation owner  
Launch provider  
SSA operator  
Insurer  
Regulatory authority  
Integrator  
Payload owner  
Groundstation provider  


**Scope**

*Add scope of use case*

Example 1: Beacon hardware

Example 2: Communications protocol


**Scenario**

*Add scenario of use case*

Example:
1. A user wants to configure the system.
2. The user changes the configuration.
3. The user restart the system.
4. The system operates with the new configuration.

/label ~"Use Case"
